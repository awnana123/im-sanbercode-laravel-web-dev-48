<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('page.register');
    }
    public function welcome(Request $request)
    {
        $namaDepan = $request->input('fname');
        $namaBelakang = $request->input('lname');
        $gender = $request->input('gender');
        $kebangsaan = $request->input('nationality');
        $bahasa = $request->input('languange');
        $pesan = $request->input('message');

        return view('page.welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
