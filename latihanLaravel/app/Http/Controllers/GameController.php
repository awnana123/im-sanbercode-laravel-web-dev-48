<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    public function create()
    {
        return view('game.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required',
        ]);

        DB::table('game')->insert([
            'name' => $request->input('name'),
            'gameplay' => $request->input('gameplay'),
            'developer' => $request->input('developer'),
            'year' => $request->input('year'),
        ]);

        return redirect('/game');
    }

    public function index()
    {
        $game = DB::table('game')->get();

        return view('game.index', ['game' => $game]);
    }

    public function show($id)
    {
        $gameData = DB::table('game')->find($id);

        return view('game.show', ['gameData' => $gameData]);
    }

    public function edit($id)
    {
        $gameData = DB::table('game')->find($id);

        return view('game.edit', ['gameData' => $gameData]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required',
        ]);

        //update
        DB::table('game')
            ->where('id', $id)
            ->update(
                [
                    'name' => $request->input('name'),
                    'gameplay' => $request->input('gameplay'),
                    'developer' => $request->input('developer'),
                    'year' => $request->input('year'),
                ]
            );

        //redirect
        return redirect('/game');
    }

    public function destroy($id)
    {
        DB::table('game')->where('id', '=', $id)->delete();

        return redirect('/game');
    }
}
