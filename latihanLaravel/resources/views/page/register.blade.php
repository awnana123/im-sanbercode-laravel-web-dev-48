@extends('layouts.master')

@section('judul')
    Register
@endsection

@section('content')
    
<form action="/welcome" method="post">
    @csrf
    <h3>Sign Up Form</h3>
    <label for="fname">First name:</label><br /><br />
    <input type="text" id="fname" name="fname" /><br /><br />
    <label for="lname">Last name:</label><br /><br />
    <input type="text" id="lname" name="lname"/><br /><br />
    <label>Gender:</label><br /><br />
    <input type="radio" value="1" name="gender" />Male<br />
    <input type="radio" value="2" name="gender" />Female<br />
    <input type="radio" value="3" name="gender" />Other<br /><br />
    <label for="select">Nationality:</label><br /><br />
    <select id="select" name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="malaysia">Malaysia</option>
        <option value="america">America</option>
    </select><br /><br />
    <label for="language_spoken">Languange Spoken:</label><br /><br />
    <input type="checkbox" value="1" name="language" />Bahasa Indonesia<br />
    <input type="checkbox" value="2" name="language" />English<br />
    <input type="checkbox" value="3" name="language" />Other<br /><br />
    <label for="bio">Bio:</label><br /><br />
    <textarea name="message" rows="10" cols="25" placeholder="Isi bio ini..."></textarea><br />
    <input type="submit" value="Sign Up" />
</form>
@endsection
