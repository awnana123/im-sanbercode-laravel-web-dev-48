@extends('layouts.master')
@section('judul')
    Tampil nama caster
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm my-3">Tambah Data</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <th scope="row">{{$key+1}}</th>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>
                    <form action="/cast/{{$item->id}}" method="post">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Update</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr><td>Tidak Ada Cast</td></tr>
        @endforelse
    </tbody>
  </table>
@endsection