@extends('layouts.master')
@section('judul')
    Tambah Cast
@endsection

@section('content')
<form action="/cast" method="post">
    @csrf
    <div class="form-group">
      <label >Nama Cast</label>
      <input name="nama" type="text" class="form-control" @error('nama') is-invalid @enderror >
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Umur</label>
      <input name="umur" type="text" class="form-control" @error('umur') is-invalid @enderror >
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Bio</label>
      <textarea name="bio" cols="30" rows="10" class="form-control" @error('bio') is-invalid @enderror ></textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection