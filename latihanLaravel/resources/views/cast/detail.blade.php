@extends('layouts.master')
@section('judul')
    Detail Cast
@endsection

@section('content')

    <h1 class="text-primary">{{$castData->nama}}</h1>
    <p>Umur: {{$castData->umur}}</p>
    <p>{{$castData->bio}}</p>
    <a href="/cast" class="btn btn-primary btn-sm">Kembali</a>
@endsection