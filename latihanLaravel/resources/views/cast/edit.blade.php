@extends('layouts.master')
@section('judul')
    Edit Cast
@endsection

@section('content')
<form action="/cast/{{$castData->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
      <label >Nama Cast</label>
      <input name="nama" value="{{$castData->nama}}" type="text" class="form-control" @error('nama') is-invalid @enderror >
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Umur</label>
      <input name="umur" value="{{$castData->umur}}" type="text" class="form-control" @error('umur') is-invalid @enderror >
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Bio</label>
      <textarea name="bio" cols="30" rows="10" class="form-control" @error('bio') is-invalid @enderror >{{$castData->bio}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection