// Variable
var nama = "Nana";
var angka = 12;
var todayIsFriday = false;

console.log(nama);
console.log(angka);
console.log(todayIsFriday);

// Operator
var mod = 10 % 3;
console.log(mod);

// Operator kondisional
console.log(true && false && false);

// string properties
var word = "Nana ganteng banget asli";
console.log(word.length);
console.log(word[0]);

// charAt
console.log("i am hero".charAt(5));

//concat
var w1 = "Nana";
var w2 = " Casmana";
console.log(w1.concat(w2));

//index of
var txt = "Na ni nu ne nooo";
console.log(txt.indexOf("Na"));

// topUpperCase
var word = "i love you and you must to know that.";
// var upper = word.toUpperCase(); // opsi 1
// console.log(upper);
console.log(word.toUpperCase()); // opsi 2

// topLowerCase
var word = "I LOVE YOU AND YOU MUST TO KNOW THAT.";
var lower = word.toLowerCase(); // opsi 1
console.log(lower);
// console.log(word.toLowerCase());
